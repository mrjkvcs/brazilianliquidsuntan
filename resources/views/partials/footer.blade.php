<!-- FOOTER
================================================== -->
<section id="footer" class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <ul class="social-icons">
                    <li class="wow bounceIn animated" data-wow-delay="0.1s"><a href="https://www.facebook.com/brazilianliquidsuntan?ref=hl" target="_blank"><i class="fa fa-facebook"></i></a></li>
                    {{--<li class="wow bounceIn animated" data-wow-delay="0.2s"><a href="#."><i class="fa fa-twitter"></i></a></li>--}}
                    {{--<li class="wow bounceIn animated" data-wow-delay="0.3s"><a href="#."><i class="fa fa-google-plus"></i></a></li>--}}
                    {{--<li class="wow bounceIn animated" data-wow-delay="0.4s"><a href="#."><i class="fa fa-dribbble"></i></a></li>--}}
                    {{--<li class="wow bounceIn animated" data-wow-delay="0.5s"><a href="#."><i class="fa fa-pinterest"></i></a></li>--}}
                </ul>
                <p class="copyright">
                    Copyrights 2015. All rights reserved.
                </p>
            </div>
        </div>
    </div>
</section>

