<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <li class="page-scroll">
                <a href="#home-slider" class="navbar-brand">
                    <img src="img/logo.png" alt="" height="50px" />
                    Brazilian Liquid Sun Tan
                </a>
            </li>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav pull-right">
                <li class="page-scroll"><a href="#home-slider">Home</a></li>
                <li class="page-scroll"><a href="#about">About</a></li>
                <li class="page-scroll"><a href="#services">Benefits</a></li>
                {{--<li class="page-scroll"><a href="#team">Team</a></li>--}}
                <li class="page-scroll"><a href="#portfolio">Services</a></li>
                <li class="page-scroll"><a href="#clients-slider">PREP & MAINT</a></li>
                <li class="page-scroll"><a href="#pricing">Pricing</a></li>
                <li class="page-scroll"><a href="#contact-page">Contact</a></li>
            </ul>
        </div>
        <!--/.nav-collapse -->
    </div>
</div>
