@extends('app')

@section('content')

    <!-- INTRO - HOMEPAGE
    ================================================== -->
    @include('sections.home')

    <!-- ABOUT
    ================================================== -->
    @include('sections.about')

    <!-- SERVICES
    ================================================== -->
    @include('sections.service')

    <!-- TEAM
    ================================================== -->
    {{--@include('sections.team')--}}

    <!-- PORTFOLIO
    ================================================== -->
    @include('sections.portfolio')

    <!-- CLIENTS (TESTIMONIALS)
    ================================================== -->
    @include('sections.client')

    <!-- PRICING
    ================================================== -->
    @include('sections.price')

    <!-- CONTACT
    ================================================== -->
    @include('sections.contact')

@endsection
