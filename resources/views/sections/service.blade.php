<section id="services">
    <div class="container">
        <div class="starter-template">
            <h1>Why Our Company is the best?</h1>
            <div class="smallsep">
            </div>
            <p class="lead">
                Because Our solution are contains natural, organic ingredients, loaded with anti-oxidants and has no added alcohol. This formula is non-sticky and lightly citrus-scented. It contains Green Tea Leaf Extract and organic Coffee Seed Leaf Extract for skin firming and toning abilities. Your skin will love the hydrating and moisturizing effects of organic Aloe Vera, an anti-irritant and organic Jojoba Seed Oil. Also, lifts and firms skin by promoting collagen and elastin production while stimulating fibroblast cells in the skin, enabling them to significantly reduce the appearance of wrinkles. A variety of pure botanical ingredients are added for anti-aging benefits.
                You can choose from Our carefully mixed and tested solutions in different colors, such as light, medium, dark and pro dark colors.
                Our unique solutions will give you a beautiful bronze sun tanned color on your skin like when you just came back from a sunny beach vacation!
            </p>
            {{--<div class="col-md-12">--}}
                {{--<div class="boxservice wow flipInY animated" data-wow-delay="0.1s">--}}
                    {{--<i class="fa fa-globe"></i>--}}
                    {{--<h3>Web Design</h3>--}}
                    {{--<p>--}}
                        {{--Web design encompasses many different skills and disciplines in the production and maintenance of websites.--}}
                    {{--</p>--}}
                {{--</div>--}}
                {{--<div class="boxservice wow flipInY animated" data-wow-delay="0.2s">--}}
                    {{--<i class="fa fa-user"></i>--}}
                    {{--<h3>User Experience</h3>--}}
                    {{--<p>--}}
                        {{--The different areas of web design include web graphic design; interface design; authoring; user experience design; and seo.--}}
                    {{--</p>--}}
                {{--</div>--}}
                {{--<div class="boxservice rightb wow flipInY animated" data-wow-delay="0.3s">--}}
                    {{--<i class="fa fa-dashboard"></i>--}}
                    {{--<h3>Client Side</h3>--}}
                    {{--<p>--}}
                        {{--The term web design is normally used to describe the design process relating to the front-end (client side) design of a website--}}
                    {{--</p>--}}
                {{--</div>--}}
                {{--<div class="boxservice bottomb wow flipInY animated" data-wow-delay="0.4s">--}}
                    {{--<i class="fa fa-cloud-download"></i>--}}
                    {{--<h3>Themes &amp; Templates</h3>--}}
                    {{--<p>--}}
                        {{--A web template system uses a template processor to combine web templates to form finished web pages.--}}
                    {{--</p>--}}
                {{--</div>--}}
                {{--<div class="boxservice bottomb wow flipInY animated" data-wow-delay="0.5s">--}}
                    {{--<i class="fa fa-trophy"></i>--}}
                    {{--<h3>Publishing Tools</h3>--}}
                    {{--<p>--}}
                        {{--It is a web publishing tool present in content management systems, web application frameworks, and HTML editors.--}}
                    {{--</p>--}}
                {{--</div>--}}
                {{--<div class="boxservice rightb bottomb wow flipInY animated" data-wow-delay="0.6s">--}}
                    {{--<i class="fa fa-microphone"></i>--}}
                    {{--<h3>Static &amp; Dynamic</h3>--}}
                    {{--<p>--}}
                        {{--Web templates can be used like the template of a form letter to generate "static" web pages in advance or to produce "dynamic" web pages.--}}
                    {{--</p>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>
</section>
