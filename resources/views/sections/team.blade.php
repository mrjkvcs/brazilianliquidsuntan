<section id="team" class="maincolorbg">
    <div class="container">
        <div class="starter-template">
            <h1>Creative Team</h1>
            <div class="smallsep">
            </div>
            <p class="lead">
                We are Hypnos, strong winners ready to meet you.<br/>
                Get in touch with us, you are a click away.

            </p>
        </div>
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="thumbnail wow bounceIn animated" data-wow-delay="0.1s" data-wow-duration="2s">
                    <img src="http://www.wowthemes.net/demo/calypso/img/demo/team1.jpg" alt="">
                    <div class="caption">
                        <h4>Ralph P. Peters</h4>
                        <span class="primarycol">- Manager at Company -</span>
                        <p>
                            Praesent id metus ante, ut condimentum magna. Nam bibendum, felis eget.<br>
                        </p>
                        <ul class="social-icons">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
