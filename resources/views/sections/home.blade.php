<section id="home-slider" class="flexslider fullbg" style="background-image:url(/img/main4.jpg); height:600px;padding:0px;">
    <ul class="slides">
        <li class="home-slide">
            <div class="flex-caption transparent light-font center">
                <p class="home-slide-content bounceInLeft animated" data-wow-delay="0.5s" data-wow-duration="4s">
                    Your <span class="highlight">Customized</span>
                </p>
                <p class="home-slide-content bounceIn animated" data-wow-delay="2s" data-wow-duration="4s">
                    Mobile <span class="highlight">Spray</span> Tan
                </p>
                <p><h3>Miami -- Las Vegas -- Houston</h3></p>
            </div>
        </li>
        {{--<li class="home-slide">--}}
            {{--<div class="flex-caption transparent light-font center">--}}
                {{--<p class="home-slide-content swing animated" data-wow-delay="2s" data-wow-duration="4s">--}}
                    {{--Infinite <span class="highlight">Skin Colors</span>--}}
                {{--</p>--}}
                {{--<p class="home-slide-content fadeInUp animated" data-wow-delay="2s" data-wow-duration="4s" style="font-size:80px;">--}}
                    {{--<span class="highlight">Lots</span> of animations--}}
                {{--</p>--}}
            {{--</div>--}}
        {{--</li>--}}
        {{--<li class="home-slide">--}}
            {{--<div class="flex-caption transparent light-font center">--}}
                {{--<p class="home-slide-content flipInY animated">--}}
                    {{--<span class="highlight">Easy</span> to Customize--}}
                {{--</p>--}}
                {{--<p class="home-slide-content slideInLeft animated">--}}
                    {{--Clean & <span class="highlight">Modern</span>--}}
                {{--</p>--}}
            {{--</div>--}}
        {{--</li>--}}
    </ul>
</section>
