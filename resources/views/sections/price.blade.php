<section id="pricing">
    <div class="container">
        <div class="starter-template">
            <h1>Pricing</h1>
            <div class="smallsep">
            </div>
            <p class="lead">
                Gift Certificate is available !
            </p>
        </div>
        <div class="row pricing">
            <div class="col-lg-3">
                <div class="whitebox salmonborder wow bounceIn animated" data-wow-delay="0.1s" data-wow-duration="2s">
                    <div class="content">
                        <p class="price">
                            45<sup>$</sup>
                        </p>
                        <ul>
                            <li>Single</li>
                        </ul>
                        {{--<a class="btn btn-lg"><i class="fa fa-shopping-cart"></i> Buy Now</a>--}}
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="whitebox goldborder wow bounceIn animated" data-wow-delay="0.2s" data-wow-duration="2s">
                    <div class="content best-offer">
                        <p class="price">
                            40<sup>$</sup>
                        </p>
                        <ul>
                            <li>2 to 5 person</li>
                        </ul>
                        {{--<a class="btn btn-lg"><i class="fa fa-user"></i> Buy Now</a>--}}
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="whitebox blueborder wow bounceIn animated" data-wow-delay="0.3s" data-wow-duration="2s">
                    <div class="content">
                        <p class="price">
                            35<sup>$</sup>
                        </p>
                        <ul>
                            <li>6 to 10 person</li>
                        </ul>
                        {{--<a class="btn btn-lg"><i class="fa fa-book"></i> Buy Now</a>--}}
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="whitebox greenborder wow bounceIn animated" data-wow-delay="0.4s" data-wow-duration="2s">
                    <div class="content">
                        <p class="price">
                            75<sup>$</sup>
                        </p>
                        <ul>
                            <li>Body Building & Fitness Competitors</li>
                        </ul>
                        {{--<a class="btn btn-lg"><i class="fa fa-mail-forward"></i> Buy Now</a>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
