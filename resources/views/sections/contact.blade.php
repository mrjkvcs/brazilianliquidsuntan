<section id="contact-page" class="maincolorbg">
    <div class="container">
        <div class="starter-template">
            <h1>Contact</h1>
            <div class="smallsep">
            </div>
            <p class="lead">
                I`ll be happy to assist you with all your questions regarding Your Customized Mobile Spray Tan !
            </p>
        </div>
            @include('errors.lists')
            {!! Form::open(['route' => 'contact_path']) !!}
            <fieldset>
                <div class="row">
                    <div class="col-md-4 wow fadeIn animated animated" data-wow-delay="0.1s" data-wow-duration="2s">
                        <label for="name" id="name">Name<span class="required">*</span></label>
                        <input type="text" class="form-control" name="name" id="name" size="30" value="" required/>
                    </div>
                    <div class="col-md-4 wow fadeIn animated" data-wow-delay="0.3s" data-wow-duration="2s">
                        <label for="email" id="email">Email<span class="required">*</span></label>
                        <input type="text" class="form-control" name="email" id="email" size="30" value="" required/>
                    </div>
                    <div class="col-md-4 wow fadeIn animated" data-wow-delay="0.5s" data-wow-duration="2s">
                        <label for="phone" id="phone">Phone</label>
                        <input type="text" class="form-control" name="phone" id="phone" size="30" value=""/>
                    </div>
                </div>
                <div class="wow fadeIn animated" data-wow-delay="0.8s" data-wow-duration="1.5" style="margin-top:15px;">
                    <label for="Message" id="message">Message<span class="required">*</span></label>
                    <textarea name="messages" class="form-control" id="message" required></textarea>
                </div>
                <br/>
                <div class="wow fadeIn animated" data-wow-delay="1s" data-wow-duration="2s">
                    <input id="submit" type="submit" name="submit" class="btn btn-primary" value="Send"/>
                </div>
            </fieldset>
        {!! Form::close() !!}
        <div id="success">
		<span>
		<p style="margin-top:20px;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            Your message was sent succssfully! I will be in touch as soon as I can.
        </p>
		</span>
        </div>
        <div id="error">
		<span>
		<p>
            Something went wrong, try refreshing and submitting the form again.
        </p>
		</span>
        </div>
    </div>
</section>
