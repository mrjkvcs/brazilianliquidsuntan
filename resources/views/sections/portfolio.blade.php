<section id="portfolio">
    <div class="container">
        <div class="starter-template">
            <h1>Services</h1>
            <div class="smallsep">
            </div>
            <p class="lead">
                Our services are perfect for anyone or for any occasion!
                Weddings, Opening Nights, Special Events, Private Party, Birthday Party, Lingerie Party, Red Carpet Events, Bikini & Body Building Competitions, Celebrities, Models, Actors, Dancers, Performers, Fashion Shows, Photo shots, TV/Movies, Holidays, Getaways or for your Every Day need …
            </p>
        </div>
        <div class="row wow bounceIn animated" data-wow-delay="0.1s" data-wow-duration="1s">
            <div class="col-sm-4 portfolio-item">
                <a href="#portfolioModal1" class="portfolio-link" data-toggle="modal">
                    <div class="caption">
                        <div class="caption-content">
                            <i class="fa fa-link fa-2x"></i>
                        </div>
                    </div>
                    <img src="/img/services/1.jpg" class="img-responsive" alt=""/>
                </a>
            </div>
            <div class="col-sm-4 portfolio-item">
                <a href="#portfolioModal2" class="portfolio-link" data-toggle="modal">
                    <div class="caption">
                        <div class="caption-content">
                            <i class="fa fa-link fa-2x"></i>
                        </div>
                    </div>
                    <img src="/img/services/2.jpg" class="img-responsive" alt=""/>
                </a>
            </div>
            <div class="col-sm-4 portfolio-item">
                <a href="#portfolioModal3" class="portfolio-link" data-toggle="modal">
                    <div class="caption">
                        <div class="caption-content">
                            <i class="fa fa-link fa-2x"></i>
                        </div>
                    </div>
                    <img src="/img/services/3.jpg" class="img-responsive" alt=""/>
                </a>
            </div>
        </div>
        <div class="row wow bounceIn animated" data-wow-delay="0.1s" data-wow-duration="1s">
            <div class="col-sm-4 portfolio-item">
                <a href="#portfolioModal4" class="portfolio-link" data-toggle="modal">
                    <div class="caption">
                        <div class="caption-content">
                            <i class="fa fa-link fa-2x"></i>
                        </div>
                    </div>
                    <img src="/img/services/4.jpg" class="img-responsive" alt=""/>
                </a>
            </div>
            <div class="col-sm-4 portfolio-item">
                <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
                    <div class="caption">
                        <div class="caption-content">
                            <i class="fa fa-link fa-2x"></i>
                        </div>
                    </div>
                    <img src="/img/services/5.jpg" class="img-responsive" alt=""/>
                </a>
            </div>
            <div class="col-sm-4 portfolio-item">
                <a href="#portfolioModal6" class="portfolio-link" data-toggle="modal">
                    <div class="caption">
                        <div class="caption-content">
                            <i class="fa fa-link fa-2x"></i>
                        </div>
                    </div>
                    <img src="/img/services/6.jpg" class="img-responsive" alt=""/>
                </a>
            </div>
        </div>
    </div>
</section>


<!-- Portfolio Modals -->
{{--<div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">--}}
    {{--<div class="modal-content">--}}
        {{--<div class="close-modal" data-dismiss="modal">--}}
            {{--<div class="lr">--}}
                {{--<div class="rl">--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-lg-8 col-lg-offset-2">--}}
                    {{--<div class="modal-body">--}}
                        {{--<h2>Project Title</h2>--}}
                        {{--<hr class="star-primary">--}}
                        {{--<img src="/img/services/1.jpg" class="img-responsive" alt=""/>--}}
                        {{--<img src="http://www.wowthemes.net/demo-vesta/wp-content/uploads/sites/12/2014/06/vestaportfolio4.jpg" class="img-responsive img-centered" alt="">--}}
                        {{--<p>--}}
                            {{--This area is used to describe your project. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.--}}
                        {{--</p>--}}
                        {{--<ul class="list-inline item-details">--}}
                            {{--<li>Client: <strong><a href="#">Hypnos</a></strong>--}}
                            {{--</li>--}}
                            {{--<li>Date: <strong><a href="#">April 2014</a></strong>--}}
                            {{--</li>--}}
                            {{--<li>Service: <strong><a href="#">Web Development</a></strong>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                        {{--<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}
{{--<div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog" aria-hidden="true">--}}
    {{--<div class="modal-content">--}}
        {{--<div class="close-modal" data-dismiss="modal">--}}
            {{--<div class="lr">--}}
                {{--<div class="rl">--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-lg-8 col-lg-offset-2">--}}
                    {{--<div class="modal-body">--}}
                        {{--<h2>Project Title</h2>--}}
                        {{--<hr class="star-primary">--}}
                        {{--<img src="http://www.wowthemes.net/demo-vesta/wp-content/uploads/sites/12/2014/06/vestaportfolio1.jpg" class="img-responsive img-centered" alt="">--}}
                        {{--<p>--}}
                            {{--This area is used to describe your project. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.--}}
                        {{--</p>--}}
                        {{--<ul class="list-inline item-details">--}}
                            {{--<li>Client: <strong><a href="#">Hypnos</a></strong>--}}
                            {{--</li>--}}
                            {{--<li>Date: <strong><a href="#">April 2014</a></strong>--}}
                            {{--</li>--}}
                            {{--<li>Service: <strong><a href="#">Web Development</a></strong>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                        {{--<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}
{{--<div class="portfolio-modal modal fade" id="portfolioModal3" tabindex="-1" role="dialog" aria-hidden="true">--}}
    {{--<div class="modal-content">--}}
        {{--<div class="close-modal" data-dismiss="modal">--}}
            {{--<div class="lr">--}}
                {{--<div class="rl">--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-lg-8 col-lg-offset-2">--}}
                    {{--<div class="modal-body">--}}
                        {{--<h2>Project Title</h2>--}}
                        {{--<hr class="star-primary">--}}
                        {{--<img src="http://www.wowthemes.net/demo-vesta/wp-content/uploads/sites/12/2014/06/vestaportfolio6.jpg" class="img-responsive img-centered" alt="">--}}
                        {{--<p>--}}
                            {{--This area is used to describe your project. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.--}}
                        {{--</p>--}}
                        {{--<ul class="list-inline item-details">--}}
                            {{--<li>Client: <strong><a href="#">Hypnos</a></strong>--}}
                            {{--</li>--}}
                            {{--<li>Date: <strong><a href="#">April 2014</a></strong>--}}
                            {{--</li>--}}
                            {{--<li>Service: <strong><a href="#">Web Development</a></strong>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                        {{--<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}
{{--<div class="portfolio-modal modal fade" id="portfolioModal4" tabindex="-1" role="dialog" aria-hidden="true">--}}
    {{--<div class="modal-content">--}}
        {{--<div class="close-modal" data-dismiss="modal">--}}
            {{--<div class="lr">--}}
                {{--<div class="rl">--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-lg-8 col-lg-offset-2">--}}
                    {{--<div class="modal-body">--}}
                        {{--<h2>Project Title</h2>--}}
                        {{--<hr class="star-primary">--}}
                        {{--<img src="http://www.wowthemes.net/demo-vesta/wp-content/uploads/sites/12/2014/06/vestaportfolio5.jpg" class="img-responsive img-centered" alt="">--}}
                        {{--<p>--}}
                            {{--This area is used to describe your project. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.--}}
                        {{--</p>--}}
                        {{--<ul class="list-inline item-details">--}}
                            {{--<li>Client: <strong><a href="#">Hypnos</a></strong>--}}
                            {{--</li>--}}
                            {{--<li>Date: <strong><a href="#">April 2014</a></strong>--}}
                            {{--</li>--}}
                            {{--<li>Service: <strong><a href="#">Web Development</a></strong>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                        {{--<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}
{{--<div class="portfolio-modal modal fade" id="portfolioModal5" tabindex="-1" role="dialog" aria-hidden="true">--}}
    {{--<div class="modal-content">--}}
        {{--<div class="close-modal" data-dismiss="modal">--}}
            {{--<div class="lr">--}}
                {{--<div class="rl">--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-lg-8 col-lg-offset-2">--}}
                    {{--<div class="modal-body">--}}
                        {{--<h2>Project Title</h2>--}}
                        {{--<hr class="star-primary">--}}
                        {{--<img src="http://www.wowthemes.net/demo-vesta/wp-content/uploads/sites/12/2014/06/vestaportfolio21.jpg" class="img-responsive img-centered" alt="">--}}
                        {{--<p>--}}
                            {{--This area is used to describe your project. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.--}}
                        {{--</p>--}}
                        {{--<ul class="list-inline item-details">--}}
                            {{--<li>Client: <strong><a href="#">Hypnos</a></strong>--}}
                            {{--</li>--}}
                            {{--<li>Date: <strong><a href="#">April 2014</a></strong>--}}
                            {{--</li>--}}
                            {{--<li>Service: <strong><a href="#">Web Development</a></strong>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                        {{--<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}
{{--<div class="portfolio-modal modal fade" id="portfolioModal6" tabindex="-1" role="dialog" aria-hidden="true">--}}
    {{--<div class="modal-content">--}}
        {{--<div class="close-modal" data-dismiss="modal">--}}
            {{--<div class="lr">--}}
                {{--<div class="rl">--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-lg-8 col-lg-offset-2">--}}
                    {{--<div class="modal-body">--}}
                        {{--<h2>Project Title</h2>--}}
                        {{--<hr class="star-primary">--}}
                        {{--<img src="http://www.wowthemes.net/demo-vesta/wp-content/uploads/sites/12/2014/06/vestaportfolio3.jpg" class="img-responsive img-centered" alt="">--}}
                        {{--<p>--}}
                            {{--This area is used to describe your project. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.--}}
                        {{--</p>--}}
                        {{--<ul class="list-inline item-details">--}}
                            {{--<li>Client: <strong><a href="#">Hypnos</a></strong>--}}
                            {{--</li>--}}
                            {{--<li>Date: <strong><a href="#">April 2014</a></strong>--}}
                            {{--</li>--}}
                            {{--<li>Service: <strong><a href="#">Web Development</a></strong>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                        {{--<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}
{{--<!-- END Portfolio Modals -->--}}
