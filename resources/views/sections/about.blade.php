<section id="about" class="graybg" style="border-top:0px;">
    <div class="container">
        @include('flash::message')
        <div class="starter-template" style="padding-bottom:0px;">
            <h1>About Us</h1>
            <div class="smallsep">
            </div>
            <p class="lead">
                Brazilian Liquid Sun Tan is a Mobile Airbrush Tanning Service, created in 2014 by the former fitness bikini competitor Timea Molnar.
            </p>
        </div>
        <p class="max80 wow bounceIn animated" data-wow-delay="0.1s">
            "My inspiration about this amazing business is to give this perfect service to anyone, so the person able to feel good, boost his/her confident, make you look good/thinner, reduce the look of cellulite, stretch marks and freckles. Doing this all in a comfortable place at your home or office. What I really like about this business is the result of happy people ! One of the best reaction i got from a customer was: ” I love my spray tan, i look so good like i just came back from the Bahamas !” …. priceless :-)
        </p>
        <br/>
        {{--<br/><img src="http://www.wowthemes.net/demo/hypnos/img/devices.png" class="img-responsive pxauto wow bounceIn animatedUp" style="margin-bottom:-60px;" data-wow-delay="0.2s">--}}
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="thumbnail wow bounceIn animated" data-wow-delay="0.1s" data-wow-duration="2s">
                    <img src="img/about_me.jpg" alt="">
                    <div class="caption">
                        <h4>Timea Molnar</h4>
                        <span class="primarycol">- Founder / Owner -</span>
                        <p>
                            <ul class="social-icons">
                                <li><a href="https://www.facebook.com/brazilianliquidsuntan?ref=hl" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                {{--<li><a href="#"><i class="fa fa-twitter"></i></a></li>--}}
                                {{--<li><a href="#"><i class="fa fa-linkedin"></i></a></li>--}}
                                {{--<li><a href="#"><i class="fa fa-google-plus"></i></a></li>--}}
                                {{--<li><a href="#"><i class="fa fa-pinterest"></i></a></li>--}}
                            </ul>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
