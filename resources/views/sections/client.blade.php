<section class="testimonial graybg" id="clients-slider">
    <div class="container">
        <div class="row">
            <div class="c12">
                <ul class="slides">
                    <li>
                        <div class="slide-text wow rollIn animated" data-wow-delay="0.1s" data-wow-duration="1s">

                            <h2><span class="uppercase">Before tanning:</span></h2>
                            <p>
                                Please shower, shave, and exfoliate before, do not apply deodorant, perfume, makeup and body lotion prior tanning, this will affect the absorption of tanning solution to your skin. Make sure you have loose clothing, no jewelry and open toed shoes. We advise you to wear dark colored bathing suits or under wear. You may also use disposable undergarments, sticky feet, disposable hat and round nipple cover up pasties are provided at the time of service at no extra charge. Although nudity is an option for females, it is not for mans. Men please wear some type of underwear.
                            </p>

                        </div>
                        <div class="slide-text">

                            <h2><span class="uppercase">After tanning:</span></h2>
                            <p>
                                Do not shower for at least 8 hours. Avoid to be exposed to any water.
                            </p>
                            <p>
                                After 8 hours take warm (not hot) shower, you will see dark tan comes off but don`t worry this is NOT your base tan. DO moisturize twice a day, special if you have dry skin. If you plan hot-tubs, swimming pools, we advise to shower off afterwards.
                                Your tan can last 7-9 days or up to two weeks as long as you take care of your skin. It is important to take care of your skin in order to achieve a longer-lasting tan. USE always body lotion more than once a day if you need it.
                                <br/>
                                Please remember to wear SPF sunscreen in the sun as spray tans do not provide any sun protection.
                            </p>
                            <p>
                                Do NOT scrub or rubbing your skin when you have the tan on, it will remove it. We are recommend to wait exercise until the first 8 hours, in this case you will sweat off your tan.
                            </p>

                        </div>
                        <div class="slide-text">
                            <h2><span class="uppercase">WHAT CAN AFFECT A SPRAY TAN?</span></h2>
                            <p>Anti-aging, smoothing, or skin renewal products can increase fading and can cause blotchiness.</p>
                            <p>Alpha Hydroxy Acids can speed exfoliation and skin turnover.</p>
                            <p>Beta Hydroxy Acids can increase fading and can lighten a tan.</p>
                            <p>Retin-A type products can increase fading and can cause blotchiness.</p>
                            <p>Toners containing high amounts of alcohol or witch hazel can fade a tan (witch hazel extract is fine).</p>
                            <p>Anti-acne products can cause faster fading, peeling, and blotchiness.</p>
                            <p>Any medication that affects the skin or skin health can also affect a tan. </p>
                            <p>Antibiotics and chemotherapy drugs can affect a tan.</p>
                            <p>Make-up remover cleaning preparations or oils can fade a tan.</p>
                            <p>Bar soaps, even moisturizing ones can be very harsh on the skin and a sunless tan.  Deodorant soaps should be avoided.  Use a SLS (Sodium Lauryl Sulfate)-free body wash for a longer lasting tan.</p>
                            <p>Some brands of lotions can cause rapid or poor fading.</p>
                            <p>Do not use a rough wash implement for daily washing (loofas, scrub mitts or scrub gloves, etc.)  Gently use a soft washcloth or your hand.</p>
                            <p>Adhesive tape, or products applied to skin, can pull the “tan” off (surgical tape, bandage, or peel off facial strips).</p>
                            <p>Hair removal procedures and depilatory products will lighten tanned skin.  Waxing will also fade or remove a tan.</p>
                            <p>Shaving exfoliates the skin so have clients use a sharp razor with a lubricating product.  Dull shaving can increase fading or cause stripes. </p>
                            <p>Body hair bleach products will also bleach the underlying skin.</p>
                            <p>Submersion in hot tubs, long hot baths, swimming pools (chlorine), fresh or salt water (a waterproof sun block helps) can lighten a tan.  Reapply a lotion after swimming and always pat dry after bathing or swimming; do not rub.</p>
                            <p>Women may “tan” differently during various hormonal changes (menstruation, pregnancy or breastfeeding).</p>
                            <p>Oil skinned clients or oil skin areas (face, chest, neck, and upper back) tan lighter than surrounding skin.</p>
                            <p>Lower legs/shins often tan lighter; therefore, more product should be applied here.</p>
                            <p>Insect repellent sprays and some perfumes, when sprayed directly onto the skin, remove or fade a tan.  Instead spray a cloud into air, and walk through the mist.</p>
                            <p>Excessive sweating and rubbing can cause tan to fade faster.</p>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
