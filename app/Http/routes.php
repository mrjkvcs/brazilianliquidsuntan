<?php

get('/', [
    'as'   => 'home',
    'uses' => 'PagesController@home'
]);

post('contact', [
    'as' => 'contact_path',
    'uses' => 'PagesController@contact'
]);

//Route::controllers([
//	'auth' => 'Auth\AuthController',
//	'password' => 'Auth\PasswordController',
//]);
