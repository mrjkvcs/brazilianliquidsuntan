<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Laracasts\Flash\Flash;

class PagesController extends Controller {

    /**
     * Home page
     *
     * @return \Illuminate\View\View
     */
    public function home()
    {
        return view('pages.home');
    }

    public function contact(Request $request)
    {
        $this->validate($request, ['email' => 'required|email', 'name' => 'required', 'phone' => 'required', 'messages' => 'required']);
        $data = $request->only(['name', 'email', 'phone', 'messages']);
        Mail::send('emails.contact', $data, function ($message)
        {
            return $message->to('timea.molnar9@gmail.com')->subject('Booking from the brazilianliquidsuntan.com');
        });

        Flash::message('Message has been sent successfully');

        return redirect()->home();
    }
}
